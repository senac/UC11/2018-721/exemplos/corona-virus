package br.com.senac.coronavirus;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import br.com.senac.coronavirus.view.CoronaActivity;
import br.com.senac.coronavirus.view.PrevencaoActivity;
import br.com.senac.coronavirus.view.TransmissaoActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        Intent intent = null;

        switch (item.getItemId()) {

            case R.id.menu_o_que_e:
                intent = new Intent(MainActivity.this, CoronaActivity.class);
                break;
            case R.id.prevencao: //PrenvencaoActivity
                intent = new Intent(MainActivity.this, PrevencaoActivity.class);
                break;
            case R.id.transmissao: //TransmissaoActivy
                intent = new Intent(MainActivity.this, TransmissaoActivity.class);
                break;

        }

        startActivity(intent);

        return true;
    }


}











